/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclisiv.cdan007.calculatrice;


/**
 *
 * @author rojoa
 */
public class Calculatrice {
    
    public static float addition(float x, float y){ 
        return x + y;
    }
    
    public static float soustraction(float x, float y){ 
        return x - y;
    }
    
    public static float multiplication(float x, float y){ 
        return x * y;
    }
    
    public  static float division(float dividend, float divisor){ 
        float result=0;
        if (divisor!=0)
        {
            result = dividend/divisor;
        }
        else 
        {
            System.out.println("Division par 0 impossible");
        }
        
        return result;
    }
    
    public double calculerMoyenne(int[] notes) {
        double somme = 0;
        for (int note : notes) {
            somme += note;
        }
        
        double moyenne = somme / notes.length;
        return moyenne;
    }

}
