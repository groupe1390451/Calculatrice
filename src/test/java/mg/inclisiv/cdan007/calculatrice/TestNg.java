package mg.inclisiv.cdan007.calculatrice;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNg {
    WebDriver driver;

    @BeforeClass
    public void setUp() {
        // Spécifiez le chemin vers votre driver (dans cet exemple, ChromeDriver)
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver-win64\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void accessSiteTest() {
        // Ouvrir le site web
        driver.get("https://www.kibo.mg/");
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/input[2]")).sendKeys("Café");
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button")).click();
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[1]/div")).click();
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button/span")).click();
        String nomProduit= driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[2]/h1")).getText();
        String quantity = driver.findElement(By.xpath("//*[@id=\"blockcart-modal\"]/div/div/div[2]/div/div[1]/div/div[2]/p[2]/text()")).getText();
        System.out.println(quantity);
        System.out.println(nomProduit);
    }

    @AfterClass
    public void tearDown() {
        // Fermer le navigateur à la fin du test
       
    }
}