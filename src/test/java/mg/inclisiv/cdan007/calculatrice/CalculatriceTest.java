/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclisiv.cdan007.calculatrice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rojoa
 */
public class CalculatriceTest {
    
    public CalculatriceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addition method, of class Calculatrice.
     */
    @org.junit.Test
    public void testAddition() {
        System.out.println("addition");
        float x = 2F;
        float y = 4F;
        float expResult = 6F;
        float result = Calculatrice.addition(x, y);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @org.junit.Test
    public void testSoustraction() {
        System.out.println("soustraction");
        float x = 8F;
        float y = 6F;
        float expResult = 2F;
        float result = Calculatrice.soustraction(x, y);
        assertEquals(expResult, result, 0.0);
        
      
    }

    /**
     * Test of multiplication method, of class Calculatrice.
     */
    @org.junit.Test
    public void testMultiplication() {
        System.out.println("multiplication");
        float x = 9.0F;
        float y = 2.0F;
        float expResult = 18.0F;
        float result = Calculatrice.multiplication(x, y);
        assertEquals(expResult, result, 0.0);
     
    }

    /**
     * Test of division method, of class Calculatrice.
     */
    @org.junit.Test
    public void testDivision() {
        System.out.println("division");
        float dividend = 9.0F;
        float divisor = 3.0F;
        float expResult = 3.0F;
        float result = Calculatrice.division(dividend, divisor);
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of calculerMoyenne method, of class Calculatrice.
     */
    @org.junit.Test
    public void testCalculerMoyenne() {
        System.out.println("calculerMoyenne");
        int[] notes = {12,15,6};
        Calculatrice instance = new Calculatrice();
        double expResult = 11;
        double result = instance.calculerMoyenne(notes);
        assertEquals(expResult, result, 0.0);
   
    }
    
}
